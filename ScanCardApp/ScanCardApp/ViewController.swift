//
//  ViewController.swift
//  ScanCardApp
//
//  Created by Sreinin on 12/1/21.
//

import UIKit
import CardScanner

class ViewController: UIViewController {
    
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBAction func didTapScanAction(_ sender: UIButton) {
        let scannerView = CardScanner.getScanner { card, date, cvv in
            //self.resultsLabel.text = "\(card) \(date) \(cvv)"
            print("Result:\("\(String(describing: card)) \(String(describing: date)) \(cvv)")")
            self.cardNumLabel.text = card ?? ""
        }
        scannerView.modalPresentationStyle = .fullScreen
        self.present(scannerView, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}


